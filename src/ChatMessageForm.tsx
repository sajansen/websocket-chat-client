import React, {Component} from 'react'
import "./ChatMessageForm.css"

interface ChatMessageFormProps {
    sendMessage: any
}

interface ChatMessageFormState {
    input: string
}

export default class ChatMessageForm extends Component<ChatMessageFormProps, ChatMessageFormState> {

    private readonly textInputElementRef: React.RefObject<HTMLInputElement> = React.createRef()

    state = {
        input: ""
    }

    inputChange = () => {
        const text = this.textInputElementRef.current!.value.trim()
        this.setState({input: text})
    }

    sendMessage = () => {
        if (this.state.input == null || this.state.input.length === 0) {
            return;
        }

        this.props.sendMessage(this.state.input)
        this.textInputElementRef.current!.value = ""
        this.textInputElementRef.current!.focus()
        this.setState({input: ""})
    }

    handleKeyUp = (e: React.KeyboardEvent<HTMLDivElement>) => {
        if (e.keyCode === 13) {
            e.preventDefault()
            this.sendMessage()
        }
    }

    scrollTo() {
        window.scroll({
            top: this.textInputElementRef.current!.offsetTop - 100,
            left: this.textInputElementRef.current!.offsetLeft - 100,
            behavior: 'smooth'
        })
    }

    componentDidMount(): void {
        this.textInputElementRef.current!.focus()
    }

    render(): React.ReactNode {
        return <div className={"ChatMessageForm"}
        onKeyUp={this.handleKeyUp}>
            <input type="text"
                   defaultValue=""
                   autoComplete={"off"}
                   placeholder={"Type message here..."}
                   ref={this.textInputElementRef}
                   onChange={this.inputChange}/>
            <button type="submit"
                    onClick={this.sendMessage}>
                Send
            </button>
        </div>
    }
}
