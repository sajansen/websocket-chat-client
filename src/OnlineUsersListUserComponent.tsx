import React, {Component} from "react";
import "./OnlineUsersListUserComponent.css"

interface OnlineUsersListUserComponentProps {
    clientName: string
    chattingWith: string
    username: string
    setChattingWith: CallableFunction
}

export default class OnlineUsersListUserComponent extends Component<OnlineUsersListUserComponentProps, any> {

    onClick() {
        if (this.props.clientName === this.props.username) {
            return
        }

        this.props.setChattingWith(this.props.username)
    }

    render(): React.ReactNode {
        return <li
            className={"OnlineUsersListUserComponent "
            + (this.props.chattingWith === this.props.username ? "OnlineUsersListUserComponent-active" : "")
            + (this.props.clientName === this.props.username ? "OnlineUsersListUserComponent-self" : "")}
            onClick={this.onClick.bind(this)}>
            {this.props.username}
            {(this.props.clientName !== this.props.username ? "" : <span className={"OnlineUsersListUserComponent-info"}>(me)</span>)}
        </li>
    }
}
