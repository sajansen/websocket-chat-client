import React, {Component} from 'react';
import './App.css';
import ChatMessageComponent from "./ChatMessageComponent";
import {ChatMessage, ChatMessageType, ClientStatus, InitializingStatus, WsActionResult, WsActionType} from "./objects";
import ChatMessageForm from "./ChatMessageForm";
import WebsocketClient from "./WebsocketClient";
import OnlineUsersListComponent from "./OnlineUsersListComponent";

interface AppState {
    clientStatus: ClientStatus
    initializationStatus: InitializingStatus
    clientName: string
    chattingWith: string
    messages: Array<ChatMessage>
    onlineUsers: Array<string>
}

class App extends Component<any, AppState> {
    ws = new WebsocketClient()
    state = {
        clientStatus: ClientStatus.INITIALIZING,
        initializationStatus: InitializingStatus.ASKING_FOR_CLIENT_NAME,
        clientName: "unknown",
        chattingWith: "server",
        messages: [],
        onlineUsers: []
    }

    private readonly chatMessageFormRef: React.RefObject<ChatMessageForm> = React.createRef()

    addMessage = (message: ChatMessage) => {
        if (!ChatMessage.isVisibleChatMessage(message)) {
            return;
        }

        this.setState((state) => ({
            messages: [...state.messages, message]
        }), () => this.chatMessageFormRef.current!.scrollTo())
    }

    sendMessage = (message: string) => {
        const chatMessage = new ChatMessage()
        chatMessage.to = this.state.chattingWith
        chatMessage.message = message
        chatMessage.type = ChatMessageType.CHAT_MESSAGE

        this.sendChatMessage(chatMessage)
    }

    sendChatMessage = (message: ChatMessage) => {
        if (message.timestamp.length === 0) {
            const now = new Date()
            message.timestamp = now.toISOString()
        }

        if (this.state.clientStatus === ClientStatus.INITIALIZING
            && this.state.initializationStatus === InitializingStatus.GETTING_CLIENT_NAME) {
            this.initialization(message)
            return
        }

        if (this.state.clientStatus === ClientStatus.CHATTING) {
            this.ws.send(message)
        }

        this.addMessage(message)
    }

    initialization(message?: ChatMessage) {
        if (this.state.clientStatus !== ClientStatus.INITIALIZING
            && this.state.initializationStatus === InitializingStatus.DONE) {
            return;
        }

        const clientBotName = "client_bot";

        if (this.state.initializationStatus === InitializingStatus.ASKING_FOR_CLIENT_NAME) {
            const botMessage1 = new ChatMessage()
            botMessage1.from = clientBotName
            botMessage1.message = "Hello there!"
            botMessage1.type = ChatMessageType.BOT_MESSAGE
            this.addMessage(botMessage1)

            const botMessage2 = new ChatMessage()
            botMessage2.from = clientBotName
            botMessage2.message = "What should we call you?"
            botMessage2.type = ChatMessageType.BOT_MESSAGE
            this.addMessage(botMessage2)

            this.setState({
                initializationStatus: InitializingStatus.GETTING_CLIENT_NAME
            })

        } else if (this.state.initializationStatus === InitializingStatus.GETTING_CLIENT_NAME) {
            if (message == null || message.from === clientBotName || message.type !== ChatMessageType.CHAT_MESSAGE) {
                return;
            }

            const newClientName = message.message;

            this.setState({
                clientName: newClientName,
                initializationStatus: InitializingStatus.CONFIRMING_CLIENT_NAME
            }, () => {
                message.from = this.state.clientName
                this.addMessage(message)
                this.initialization(undefined)
            })

        } else if (this.state.initializationStatus === InitializingStatus.CONFIRMING_CLIENT_NAME) {
            const botMessage = new ChatMessage()
            botMessage.from = clientBotName
            botMessage.message = "Alright! Happy chatting, " + this.state.clientName + "!"
            botMessage.type = ChatMessageType.CHAT_MESSAGE
            this.addMessage(botMessage)

            this.setState({
                clientStatus: ClientStatus.CHATTING,
                initializationStatus: InitializingStatus.CONNECTING_TO_SERVER
            }, this.initialization)

        } else if (this.state.initializationStatus === InitializingStatus.CONNECTING_TO_SERVER) {
            this.ws.onMessageFromServer = this.onMessageFromServer.bind(this)
            this.ws.clientName = this.state.clientName
            this.ws.connect()

            this.setState({
                initializationStatus: InitializingStatus.DONE
            })
        } else {
            console.warn("Unknown initializationStatus: " + this.state.initializationStatus)
        }
    }

    handleActionResult(message: ChatMessage) {
        if (message.type !== ChatMessageType.ACTION_RESULT) {
            return
        }

        const actionResult = WsActionResult.fromJson(message.message)
        if (actionResult.action_name === WsActionType.GET_ALL_ONLINE_USERS) {
            if (!actionResult.is_success) {
                console.error("Failed to get online users: ", actionResult.message, actionResult.exception)
                return
            }
            const usernames: Array<string> = actionResult.data.usernames
            this.setState({onlineUsers: usernames})

        } else if (actionResult.action_name === WsActionType.GET_ALL_CHAT_MESSAGES_FROM_AND_TO_USER) {
            if (!actionResult.is_success) {
                console.error("Failed to get user messages history: ", actionResult.message, actionResult.exception)
                return
            }

            const messages: Array<ChatMessage> = actionResult.data.messages.map(ChatMessage.fromJson)

            const chatMessages = messages.filter((m: ChatMessage) => m.from !== "server")
                .filter(ChatMessage.isVisibleChatMessage)
                .sort(ChatMessage.orderByTimestamp)

            // Add message history to the current list of messages and sort on timestamp
            this.setState((state) => ({
                messages: state.messages.concat(chatMessages)
            }), () => this.chatMessageFormRef.current!.scrollTo())
        }
    }

    onMessageFromServer(message: ChatMessage) {
        console.debug("Received message:")
        console.debug(message)

        if (ChatMessage.isVisibleChatMessage(message)) {
            this.addMessage(message)
        } else if (message.type === ChatMessageType.ACTION_RESULT) {
            this.handleActionResult(message);
        }
    }

    setChattingWith(username: string) {
        console.log("Changing chatting with to: " + username)
        this.setState({chattingWith: username})
    }

    componentDidMount() {
        this.initialization(undefined)
    }

    render() {
        return <div className="App">
            <header className="App-header">
                <OnlineUsersListComponent usernames={this.state.onlineUsers}
                                          clientName={this.state.clientName}
                                          chattingWith={this.state.chattingWith}
                                          setChattingWith={this.setChattingWith.bind(this)}/>
                {this.state.messages
                    .filter((message: ChatMessage) =>
                        message.type === ChatMessageType.CHAT_MESSAGE
                        || message.type === ChatMessageType.BOT_MESSAGE
                    )
                    .map((message: ChatMessage, index: number) =>
                        <ChatMessageComponent key={index} client_name={this.state.clientName} {...message} />
                    )}
                <ChatMessageForm
                    ref={this.chatMessageFormRef}
                    sendMessage={this.sendMessage}/>
            </header>
        </div>
    }
}

export default App;
