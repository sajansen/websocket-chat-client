import React, {Component} from 'react'
import "./ChatMessageComponent.css"

interface ChatMessageComponentProps {
    client_name: string
    id: number
    from: string
    message: string
    timestamp: string
}

export default class ChatMessageComponent extends Component<ChatMessageComponentProps, any> {

    render(): React.ReactNode {
        const timestampDate = new Date(this.props.timestamp)
        return <div
            className={"ChatMessageComponent ChatMessageComponent-" + (this.props.client_name === this.props.from ? "client" : "server")}>
            <div className="ChatMessageComponent-name">{this.props.from}</div>
            <div className="ChatMessageComponent-content">{this.props.message}</div>
            <div className={"ChatMessageComponent-time"}>{timestampDate.getHours()}:{timestampDate.getMinutes().toString().padStart(2, "0")}</div>
        </div>
    }
}
