import {ChatMessage, ChatMessageType, WsActionType} from "./objects";

export default class WebsocketClient {
    address: string = 'ws://localhost:3001/ws'
    clientName: string = "unknown"
    onMessageFromServer?: CallableFunction = undefined

    private ws?: WebSocket = undefined
    private connectionRetryTimeout?: number = undefined
    private connectionRetryTimoutDuration: number = 5000

    connect() {
        this.ws = new WebSocket(this.address)

        this.ws.onopen = () => {
            console.log("Connected to server")
            window.clearTimeout(this.connectionRetryTimeout)

            const serverInitMessage = new ChatMessage()
            serverInitMessage.from = this.clientName
            serverInitMessage.to = "server"
            serverInitMessage.type = ChatMessageType.CONNECTION_INIT
            this.send(serverInitMessage)

            const onlineUsersMessage = new ChatMessage()
            onlineUsersMessage.from = this.clientName
            serverInitMessage.to = "server"
            onlineUsersMessage.type = ChatMessageType.ACTION
            onlineUsersMessage.message = WsActionType.GET_ALL_ONLINE_USERS
            this.send(onlineUsersMessage)

            const allUserMessages = new ChatMessage()
            allUserMessages.from = this.clientName
            allUserMessages.to = "server"
            allUserMessages.type = ChatMessageType.ACTION
            allUserMessages.message = WsActionType.GET_ALL_CHAT_MESSAGES_FROM_AND_TO_USER
            this.send(allUserMessages)
        }

        this.ws.onclose = () => {
            console.log(`Disconnected with server. Retrying in ${Math.round(this.connectionRetryTimoutDuration / 1000.0)} seconds...`)
            this.connectionRetryTimeout = window.setTimeout(this.livenessCheck.bind(this), this.connectionRetryTimoutDuration)
        }

        this.ws.onmessage = (e) => {
            console.debug("Received data:")
            console.debug(e.data)

            let message: ChatMessage;
            try {
                message = ChatMessage.fromJson(e.data)
            } catch (e) {
                console.error("Data is not JSON serializable")
                return
            }

            if (this.onMessageFromServer) {
                this.onMessageFromServer(message);
            }
        }
    }

    disconnect() {
        this.ws?.close()
    }

    livenessCheck() {
        if (!this.ws || this.ws.readyState === WebSocket.CLOSED) {
            this.connect()
        }
    }

    send(chatMessage: ChatMessage) {
        chatMessage.from = this.clientName

        console.debug("Seding message:")
        console.debug(chatMessage)
        this.ws?.send(chatMessage.json())
    }

    sendRaw(data: any) {
        this.ws?.send(data)
    }
}
