import React, {Component} from 'react'
import OnlineUsersListUserComponent from "./OnlineUsersListUserComponent";

interface OnlineUsersListComponentProps {
    clientName: string
    chattingWith: string
    usernames: Array<string>
    setChattingWith: CallableFunction
}

export default class OnlineUsersListComponent extends Component<OnlineUsersListComponentProps, any> {

    render(): React.ReactNode {
        return <div className={"OnlineUsersListComponent"}>
            Online:
            <ul>
                {this.props.usernames.map((username, index) =>
                    <OnlineUsersListUserComponent key={index}
                                                  clientName={this.props.clientName}
                                                  chattingWith={this.props.chattingWith}
                                                  username={username}
                                                  setChattingWith={this.props.setChattingWith}/>)}
                <hr/>
                <OnlineUsersListUserComponent chattingWith={this.props.chattingWith}
                                              clientName={this.props.clientName}
                                              username={"server"}
                                              setChattingWith={this.props.setChattingWith}/>
                <OnlineUsersListUserComponent chattingWith={this.props.chattingWith}
                                              clientName={this.props.clientName}
                                              username={"all"}
                                              setChattingWith={this.props.setChattingWith}/>
            </ul>
        </div>
    }
}
