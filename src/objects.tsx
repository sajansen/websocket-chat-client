export enum ClientStatus {
    INITIALIZING,
    CHATTING
}

export enum InitializingStatus {
    ASKING_FOR_CLIENT_NAME,
    GETTING_CLIENT_NAME,
    CONFIRMING_CLIENT_NAME,
    CONNECTING_TO_SERVER,
    DONE
}

export enum ChatMessageType {
    UNKNOWN = "UNKNOWN",
    CONNECTION_INIT = "CONNECTION_INIT",
    CONNECTION_CLOSE = "CONNECTION_CLOSE",
    CHAT_MESSAGE = "CHAT_MESSAGE",
    BOT_MESSAGE = "BOT_MESSAGE",
    ACTION = "ACTION",
    ACTION_RESULT = "ACTION_RESULT",
}

export enum WsActionType {
    GET_ALL_ONLINE_USERS = "GET_ALL_ONLINE_USERS",
    GET_ALL_CHAT_MESSAGES_FROM_AND_TO_USER = "GET_ALL_CHAT_MESSAGES_FROM_AND_TO_USER"
}

export class ChatMessage {
    id: number = 0;
    from: string = "";
    to: string = "";
    message: string = "";
    timestamp: string = (new Date()).toISOString();
    type: ChatMessageType = ChatMessageType.UNKNOWN;

    json() {
        return JSON.stringify(this)
    }

    static fromJson(json: string): ChatMessage {
        const object = JSON.parse(json)
        const chatMessage = new ChatMessage()
        chatMessage.id = object.id
        chatMessage.from = object.from
        chatMessage.to = object.to
        chatMessage.message = object.message
        chatMessage.timestamp = object.timestamp
        chatMessage.type = object.type
        return chatMessage
    }

    static isVisibleChatMessage  = (m: ChatMessage) =>
        m.type === ChatMessageType.BOT_MESSAGE || m.type === ChatMessageType.CHAT_MESSAGE

    static orderByTimestamp = (a: ChatMessage, b: ChatMessage) => a.timestamp.localeCompare(b.timestamp)
}

export class WsActionResult {
    action_name: string = "";
    is_success: boolean = false;
    message: string = "";
    data?: any = undefined;
    exception: string = "";

    static fromJson(json: string): WsActionResult {
        const object = JSON.parse(json)
        const actionResult = new WsActionResult()
        actionResult.action_name = object.action_name
        actionResult.is_success = object.is_success
        actionResult.message = object.message
        actionResult.data = object.data ? object.data : undefined
        actionResult.exception = object.exception
        return actionResult
    }
}
